# Cache Coherence Protocol Simulator
	
Dependencies:
=============
- gcc 4.7.2 (or other version with c++11 support)
	
	
Usage:
run
> make
> ./dist/Release/GNU-Linux-x86/assignment2 [args]
 - OR -
open with NetBeans to compile, then run as above.
 - OR -
for a 32-bit binary, compile with
> make CONF=Release32
and run:
> ./dist/Release32/GNU-Linux-x86/assignment2 [args]

Accepted invocation format:
---------------------------
assignment2 -p [coherence-protocol] -l [number-of lines] -s [line-size] [input-file]

Accepted values for -w flag are: 1, 2, 4, 8, 16.
Accepted values for -s flag are: 1, 2, 4, 8, ..., 4096. Larger numbers
  (only in powers of 2) are accepted by the program, but you may run into
  undefined behaviour.

For a similar help file, run
> ./dist/Release/GNU-Linux-x86/assignment2 -h


Note that there is a symlink to the executable in the project's root folder.
