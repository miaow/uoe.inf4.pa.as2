/*
 * File:   CSimulator.cpp
 * Author: Mihail Atanassov, s1018353
 *
 * Created on February 9, 2013, 6:41 PM
 */
//#define NDEBUG
#include "include/CSimulator.h"
#include <cstdlib>
#include <cassert>

/* special commands */
#define VERBOSE 252
#define PRINTME 253
#define PRINTHITS 254
#define PRINTINVALS 255

char state_string[4][8] = {"STATE_I", "STATE_S", "STATE_M", "STATE_E"};

using namespace std;

CSimulator::CSimulator( ) :
m_proto( PROTO_UNDEF ),
m_str_input_file( NULL ),
m_verbose( false )
{
	/* initialise the stats strut */
	this->m_stats = ( results_struct ){0};
// 	for( int i = 0; i < NUM_CPU; ++i )
// 	{
// 		m_stats.reads[i] = 0;
// 		m_stats.writes[i] = 0;
// 		m_stats.invalidations[i] = 0;
// 		m_stats.read_hits[i] = 0;
// 		m_stats.write_hits[i] = 0;
// 		m_stats.read_miss[i] = 0;
// 		m_stats.write_miss[i] = 0;
// 		m_stats.evictions[i] = 0;
// 		m_stats.privates[i] = 0;
// 		m_stats.shared_ro[i] = 0;
// 		m_stats.shared_rw[i] = 0;
//
// 		m_stats.read_miss_rate[i] = 0.0L;
// 		m_stats.write_miss_rate[i] = 0.0L;
// 		m_stats.invalidation_rate[i] = 0.0L;
// 		m_stats.eviction_rate[i] = 0.0L;
// 		m_stats.privates_rate[i] = 0.0L;
// 		m_stats.shared_ro_rate[i] = 0.0L;
// 		m_stats.shared_rw_rate[i] = 0.0L;
// 	}
//
// 	m_stats.cache_size = 0.0L;
// 	m_stats.num_lines = 0;
// 	m_stats.line_size = 0;
// 	m_stats.total_reads = 0;
// 	m_stats.total_writes = 0;
// 	m_stats.total_invals = 0;
// 	m_stats.total_read_hits = 0;
// 	m_stats.total_read_miss = 0;
// 	m_stats.total_write_hits = 0;
// 	m_stats.total_write_miss = 0;
// 	m_stats.total_evictions = 0;
// 	m_stats.total_privates = 0;
// 	m_stats.total_shared_ro = 0;
// 	m_stats.total_shared_rw = 0;
//
// 	m_stats.total_read_miss_rate = 0;
// 	m_stats.total_read_hit_rate = 0;
// 	m_stats.total_write_miss_rate = 0;
// 	m_stats.total_write_hit_rate = 0;
//
// 	m_stats.protocol = NULL;
}

CSimulator::~CSimulator( ) { }

/* checks for a power of two, courtesy of glibc */
inline bool pow2( uint32_t x )
{
	return ( x > 0 && !( x & (x-1) ) );
}

bool CSimulator::init( int &argc, char** argv )
{
	switch ( init_( argc, argv ) )
	{
		case INIT_OK:
			return true;
		case INIT_NOFILE:
			printf( "No input file.\n" );
			return false;
		case INIT_BAD_NUMLINES:
			printf( "The number of lines is not a power of 2\n" );
			return false;
		case INIT_BAD_LINESIZE:
			printf( "The line size is not a power of 2.\n");
			return false;
		case INIT_BAD_PROTO:
			fprintf( stderr, "Unknown protocol. Use msi or mesi.\n" );
			return false;
		case INIT_OPT_WO_ARG:
			printf( "Option without an argument.\n" );
			return false;
		case INIT_UNKNOWN_OPT:
			fprintf( stderr, "Unknown option.\n" );
			return false;
		case INIT_HELP:
			exit( EXIT_SUCCESS );
		default:
			return false;
	}
}

int CSimulator::init_( int &argc, char** argv )
{
	const char * const optStr = ":hp:l:s:";
	int c = 0;
	while ( ( c = getopt( argc, argv, optStr ) ) != -1 )
	{
		switch ( c )
		{
			case 'h':
				printf( "Usage:\n\
assignment2 -p <protocol> -l <num lines> -s <line size> <input-file>\n\
Accepted protocols are: msi, mesi.\n\
Datafile format:\tP# R 0123abcd45de\n\t\t\tW 0123abcd45de\n" );
				return INIT_HELP;
			case 'p':
				m_stats.protocol = optarg;
			case 'l': /* number of ways */
				m_stats.num_lines = (uint32_t) strtoumax( optarg, NULL, 10 );
				break;
			case 's': /* number of sets */
				m_stats.line_size = (uint32_t) strtoumax( optarg, NULL, 10 );
				break;
			case ':':
				return INIT_OPT_WO_ARG;
				break;
			case '?':
				return INIT_UNKNOWN_OPT;
				break;
			default:
				break;
		}
	}

	/* check if the protocol string is valid */
	if( !strcmp(m_stats.protocol, "msi") )
		m_proto = PROTO_MSI;
	if( !strcmp(m_stats.protocol, "mesi") )
		m_proto = PROTO_MESI;
	if( m_proto == PROTO_UNDEF )
		return INIT_BAD_PROTO;

	/* check if the number of lines is a power of 2 */
	if(!pow2(m_stats.num_lines))
		return INIT_BAD_NUMLINES;
	/* ditto for line size */
	if(!pow2(m_stats.line_size))
		return INIT_BAD_LINESIZE;

	/* Opts are parsed, now we need the input filename */
	if ( argv[optind] == NULL )
		return INIT_NOFILE;

	m_str_input_file = argv[optind];
	return INIT_OK;
}

bool CSimulator::read_file( )
{
	/* gcc & the C way: 5 times faster than using stringstream etc. */
	FILE* fh = NULL;
	if ( ( fh = fopen( m_str_input_file, "r" ) ) == NULL )
		return false; /* File does not exist? */

	char line[32]; /* line character limit */
	uint64_t addr = 0;
	bool rw = READ;
	uint8_t cpu = 0;
	while ( fgets( line, sizeof line, fh ) != NULL ) /* parse each line */
	{
		/* add handling for special instructions in the data stream */
		if( line[0] == 'v' )
		{
			/* toggle verbose output */
			m_data.push_back( make_tuple( VERBOSE, 0, 0 ) );
			continue;
		}
		if( line[0] == 'p' )
		{
			/* print cache contents */
			m_data.push_back( make_tuple( PRINTME, 0, 0 ) );
			continue;
		}
		if( line[0] == 'h' )
		{
			/* print hit rate so far */
			m_data.push_back( make_tuple( PRINTHITS, 0, 0 ) );
			continue;
		}
		if( line[0] == 'i' )
		{
			/* print # of  invalidations so far */
			m_data.push_back( make_tuple( PRINTINVALS, 0, 0 ) );
			continue;
		}

		/* line format: P<cpu#> <R|W> <addr> */
		cpu = line[1] - '0';
		if ( line[3] == 'W' ) /* read or write? */
			rw = WRITE;
		else if( line[3] == 'R' )
			rw = READ;
		else return false;

#if defined __x86_64__ | WIN64 == 1 /* get address */
		if ( !sscanf( line + 5, "%lu", &addr ) )
		{
			fclose( fh );
			return false; /* bad data */
		}
#else
		if ( !sscanf( line + 5, "%llu", &addr ) )
		{
			fclose( fh );
			return false; /* bad data */
		}
#endif
		m_data.push_back( make_tuple( cpu, addr, rw ) ); /* store it */
	}
	fclose( fh );
	if ( m_data.empty( ) )
		return false; /* the file did not have anything in it */

	return true;
}

const uint64_t CSimulator::get_data_size( ) const
{
	return m_data.size( );
}

const char* CSimulator::get_input_filename( ) const
{
	return m_str_input_file;
}

const results_t* CSimulator::get_results( ) const
{
	return &m_stats;
}

void CSimulator::run( )
{
	/* first, initialise the cache vectors */
	for( int i = 0; i < NUM_CPU; ++i )
	{
		m_cache[i].resize(m_stats.num_lines);
	}
	uint8_t cpu = 0;
	uint64_t addr = 0;
	bool rw = READ;
	uint64_t cacheAddr = 0;
	uint64_t cacheWord = 0;
	uint64_t cacheTag  = 0;


	uint64_t offset_mask = m_stats.line_size - 1;
	uint64_t index_mask = ( m_stats.num_lines * m_stats.line_size - 1 ) &
						  ~offset_mask;
	uint64_t tag_mask = ~( m_stats.num_lines * m_stats.line_size - 1 );

	/* run the simulation following the trace file */
	for( m_data_iterator = m_data.begin( );
		m_data_iterator != m_data.end( );
		++m_data_iterator )
	{
		uint32_t curr_line = (uint32_t) (m_data_iterator - m_data.begin() + 1);

		/* grab the trace data and compute the tag, index, and offset */
		tie( cpu, addr, rw ) = *m_data_iterator;
		cacheAddr = ( addr & index_mask ) / m_stats.line_size;
		cacheWord = addr & offset_mask;
		cacheTag  = ( addr & tag_mask ) /
					( m_stats.line_size * m_stats.num_lines );

		/* check if the line was a command rather than a part of the trace
		 * proper
		 */
		if( handle_commands( cpu, curr_line ) )
			continue;

		/* it wasn't a command, so check if the cpu num is out of bounds */
		assert( cpu < NUM_CPU );

		/* update the number of reads and writes */
		if( rw == READ )
			++m_stats.reads[cpu];
		else
			++m_stats.writes[cpu];

		/* check that cpu's state of the target line/tag combination
		 * and adjust accordingly
		 */
		if( m_cache[cpu][cacheAddr].tag != cacheTag )
		{
			/* the tags don't match - eviction */
			if( m_cache[cpu][cacheAddr].used )
				++m_stats.evictions[cpu];
			if( rw == READ )
				++m_stats.read_miss[cpu];
			else
				++m_stats.write_miss[cpu];
			this->inform(cpu, rw, cacheAddr, cacheTag);
			if( m_proto == PROTO_MESI )
			{
				/* if the data is cached in another cpu, go to shared, else
				 * go to exclusive state */
				if( this->inform(cpu, rw, cacheAddr, cacheTag) )
					m_cache[cpu][cacheAddr].state = ( rw == READ ) ?
													STATE_S : STATE_M;
				else
				{
					m_cache[cpu][cacheAddr].state = ( rw == READ ) ?
													STATE_E : STATE_M;
				}
			}
			else
			{
				this->inform(cpu, rw, cacheAddr, cacheTag);
				m_cache[cpu][cacheAddr].state = ( rw == READ ) ?
													STATE_S : STATE_M;
			}
			m_cache[cpu][cacheAddr].tag = cacheTag;
			m_cache[cpu][cacheAddr].last_used_word = cacheWord;
			m_cache[cpu][cacheAddr].used = true;
		}
		else
		{
			/* tags match - check the state of the line and update if needed */
			switch( m_cache[cpu][cacheAddr].state )
			{
				case STATE_I:
					/* state is invalid, either not used before or invalidated
					 * from previous operation. In any case, simply write over
					 * the data
					 */
					if( rw == READ )
						++m_stats.read_miss[cpu];
					else
						++m_stats.write_miss[cpu];
					this->inform(cpu, rw, cacheAddr, cacheTag);
					m_cache[cpu][cacheAddr].state = STATE_S;
					m_cache[cpu][cacheAddr].tag = cacheTag;
					m_cache[cpu][cacheAddr].last_used_word = cacheWord;
					m_cache[cpu][cacheAddr].used = true;
					break;
				case STATE_S:
					/* if write, it's a miss. if read, do it silently */
					if( rw == WRITE )
					{
						++m_stats.write_miss[cpu];
						this->inform( cpu, rw, cacheAddr, cacheTag );
						m_cache[cpu][cacheAddr].state = STATE_M;
						m_cache[cpu][cacheAddr].tag = cacheTag;
					}
					else
					{
						++m_stats.read_hits[cpu];
					}
					m_cache[cpu][cacheAddr].last_used_word = cacheWord;
					break;
				case STATE_M:
					/* hit: remain modified and operate silently, read or write
					 */
					if( rw == READ )
						++m_stats.read_hits[cpu];
					else
						++m_stats.write_hits[cpu];
					m_cache[cpu][cacheAddr].last_used_word = cacheWord;
					break;
				case STATE_E:
					if( m_proto == PROTO_MESI )
					{
						if( rw == READ )
							++m_stats.read_hits[cpu];
						else
						{
							++m_stats.write_hits[cpu];
							m_cache[cpu][cacheAddr].state = STATE_M;
						}
					}
					else
					{
						printf("INVALID STATE IN PROTO_MSI\n\n");
						exit(EXIT_FAILURE);
					}
					break;
				default:
					printf("Go to a room with less ionising radiation! " );
					printf( "It's bad for your health!\n\n");
					exit(EXIT_FAILURE);
					break;
			}
		}
	}

	/* the run is complete, calculate metrics */
	this->calc_stats();
}

void CSimulator::calc_stats( )
{
	/* calculate all the derived metrics */
	for( int i = 0; i < NUM_CPU; ++i )
	{
		m_stats.read_miss_rate[i] = ( m_stats.reads[i] == 0 ) ?
			0.0L :
			m_stats.read_miss[i] * 100.0L / m_stats.reads[i];

		m_stats.write_miss_rate[i] = ( m_stats.writes[i] == 0 ) ?
			0.0L :
			m_stats.write_miss[i] * 100.0L / m_stats.writes[i];

		m_stats.invalidation_rate[i] = ( m_stats.writes[i] == 0 ) ?
			0.0L :
			m_stats.invalidations[i] * 100.0L / m_stats.writes[i];

		m_stats.eviction_rate[i] = ( m_stats.reads[i] + m_stats.writes[i] == 0 )
			? 0.0L :
			m_stats.evictions[i] * 100.0L / ( m_stats.reads[i] +
											  m_stats.writes[i] );

		m_stats.privates_rate[i] = ( m_stats.reads[i] + m_stats.writes[i] == 0 )
			? 0.0L :
			m_stats.privates[i] * 100.0L / ( m_stats.reads[i]
										   + m_stats.writes[i] );
		m_stats.shared_ro_rate[i] = ( m_stats.reads[i] + m_stats.writes[i] == 0 )
			? 0.0L :
			m_stats.shared_ro[i] * 100.0L / ( m_stats.reads[i]
											+ m_stats.writes[i] );
		m_stats.shared_rw_rate[i] = ( m_stats.reads[i] + m_stats.writes[i] == 0 )
			? 0.0L :
			m_stats.shared_rw[i] * 100.0L / ( m_stats.reads[i]
											+ m_stats.writes[i] );

		/* totals */
		m_stats.total_reads += m_stats.reads[i];
		m_stats.total_writes += m_stats.writes[i];
		m_stats.total_invals += m_stats.invalidations[i];
		m_stats.total_read_hits += m_stats.read_hits[i];
		m_stats.total_read_miss += m_stats.read_miss[i];
		m_stats.total_write_hits += m_stats.write_hits[i];
		m_stats.total_write_miss += m_stats.write_miss[i];
		m_stats.total_evictions += m_stats.evictions[i];
		m_stats.total_privates += m_stats.privates[i];
		m_stats.total_shared_ro += m_stats.shared_ro[i];
		m_stats.total_shared_rw += m_stats.shared_rw[i];
	}
	m_stats.cache_size = m_stats.num_lines * m_stats.line_size >> 10;

	m_stats.total_read_miss_rate = ( m_stats.total_reads == 0 ) ?
		0.0L :
		m_stats.total_read_miss * 100.0L / m_stats.total_reads;

	m_stats.total_write_miss_rate = ( m_stats.total_writes == 0 ) ?
		0.0L :
		m_stats.total_write_miss * 100.0L / m_stats.total_writes;

	m_stats.total_invalidation_rate = ( m_stats.total_writes == 0 ) ?
		0.0L :
		m_stats.total_invals * 100.0L / m_stats.total_writes;

	m_stats.total_eviction_rate = ( m_stats.total_reads +
									m_stats.total_writes == 0 )
		? 0.0L :
		m_stats.total_evictions * 100.0L / ( m_stats.total_reads +
											 m_stats.total_writes );

	m_stats.total_privates_rate = ( m_stats.total_reads +
									m_stats.total_writes == 0 )
		? 0.0L :
		m_stats.total_privates * 100.0L / ( m_stats.total_reads +
											m_stats.total_writes );

	m_stats.total_shared_ro_rate = ( m_stats.total_reads +
									 m_stats.total_writes == 0 )
		? 0.0L :
		m_stats.total_shared_ro * 100.0L / ( m_stats.total_reads +
											 m_stats.total_writes );

	m_stats.total_shared_rw_rate = ( m_stats.total_reads +
									 m_stats.total_writes == 0 )
		? 0.0L :
		m_stats.total_shared_rw * 100.0L / ( m_stats.total_reads +
											 m_stats.total_writes );
}

bool CSimulator::handle_commands( uint8_t cmd, uint32_t curr_line )
{
	if( cmd == VERBOSE )
	{
		m_verbose ^= 1; /* toggle verbose mode */
		return true;
	}
	if( cmd == PRINTME )
	{
		/* print cache contents */
		printf( "Cache dump request at line %u\n\n", curr_line );
		for( int i = 0; i < NUM_CPU; ++i )
		{
			printf( "\tCache %1d\n\n", i );
			printf( "%8s\t%18s\t%10s\n", "Line", "Tag", "State" );
			for( int j = 0; j < m_stats.num_lines; ++j )
			{
				printf( "%8u\t0x%016lx\t%10s\n",
						j,
						m_cache[i][j].tag,
						state_string[m_cache[i][j].state] );
			}
			printf( "\n" );
		}
		return true;
	}
	if( cmd == PRINTHITS )
	{
		printf( "Hit rate request at line %u\n\n", curr_line );
		for( int i = 0; i < NUM_CPU; ++i )
		{
			printf( "\tCache %1d\n\n", i );
			double read_hit_rate = ( m_stats.reads[i] == 0 ) ? 0.0L :
				(double) ( m_stats.read_hits[i] * 100.0L / m_stats.reads[i] );
			double write_hit_rate = ( m_stats.writes[i] == 0 ) ? 0.0L :
				(double) ( m_stats.write_hits[i] * 100.0L / m_stats.writes[i] );
			printf( "Read hits:\t\t%10d of %10d; %8.4lf %%\n",
					m_stats.read_hits[i],
					m_stats.reads[i],
					read_hit_rate );
			printf( "Write hits:\t\t%10d of %10d; %8.4lf %%\n\n",
					m_stats.write_hits[i],
					m_stats.writes[i],
					write_hit_rate );
		}
		return true;
	}
	if( cmd == PRINTINVALS )
	{
		printf( "Invalidation count request at line %u\n\n", curr_line );
		for( int i = 0; i < NUM_CPU; ++i )
		{
			printf( "\tCache %1d:\n\nInvalidations:\t%10d\n\n",
					i,
					m_stats.invalidations[i] );
		}
		return true;
	}
	return false;
}

bool CSimulator::inform( uint8_t informer,
						 bool rw,
						 uint64_t index,
						 uint64_t tag )
{
	bool retval = false;
	for( int i = 0; i < NUM_CPU; ++i )
	{
		if( i == informer )
			continue;
		/* if the tags don't match, we move on */
		if( m_cache[i][index].tag != tag )
			continue;
		/* only here if the tags match - check state and act accordingly */
		switch( m_cache[i][index].state )
		{
			case STATE_I: /* nothing interesting */
				break;
			case STATE_S: /* if a read, stay in state. if a write, inval */
				retval = true;
				if( rw == WRITE )
				{
					++m_stats.invalidations[i];
					m_cache[i][index].state = STATE_I;
					m_cache[i][index].used = false;
				}
				break;
			case STATE_M: /* if read, move to shared. if write, move to inv */
				retval = true;
				if( rw == READ )
					m_cache[i][index].state = STATE_S;
				else
				{
					++m_stats.invalidations[i];
					m_cache[i][index].state = STATE_I;
					m_cache[i][index].used = false;
				}
				break;
			case STATE_E:
				if( m_proto == PROTO_MESI )
				{
					if( rw == READ )
						m_cache[i][index].state = STATE_S;
					else
					{
						++m_stats.invalidations[i];
						m_cache[i][index].state = STATE_I;
						m_cache[i][index].used = false;
					}
				}
				else
				{
					printf("INVALID STATE IN PROTO_MSI\n\n");
					exit(EXIT_FAILURE);
				}
				break;
			default:
				printf("Go to a room with less ionising radiation! " );
				printf( "It's bad for your health!\n\n");
				exit(EXIT_FAILURE);
				break;
		}
	}
	return retval;
}
