/*
 * File:   CSimulator.h
 * Author: Mihail Atanassov, s1018353
 *
 * Created on February 9, 2013, 6:41 PM
 */

#ifndef CSIMULATOR_H
#define	CSIMULATOR_H

using namespace std;

/* includes */
#include<unistd.h>		/* getopt */
#include<inttypes.h>	/* (u)intXX_t */
#include<cstdio>		/* printf */
#include<cstring>		/* strcmp */
#include<vector>		/* std::vector */
#include<tuple>			/* std::tuple */
#include<map>			/* std::map */
#include<list>			/* std::list */
#include<cmath>			/* log2() */

/* return codes */
#define INIT_OK				0	/* Initialisation OK */
#define INIT_NOFILE			1	/* Input file not given */
#define INIT_BAD_NUMLINES	2	/* Number of lines not a power of 2 */
#define INIT_BAD_LINESIZE	3	/* Line size not a power of 2 */
#define INIT_OPT_WO_ARG		4	/* Option had no argument */
#define INIT_UNKNOWN_OPT	5	/* Option unknown */
#define INIT_HELP			6	/* Help invoked */
#define INIT_BAD_PROTO		7	/* protocol string is unknown */

/* whether we have a read or a write access */
#define WRITE				true
#define READ				false

/* cache-related things */
#define BYTEOFFSET			5	/* 5 bits for the byte offset part */

/* protocol macros */
#define PROTO_UNDEF			0
#define PROTO_MSI			1
#define PROTO_MESI			2

/* recompile with a larger number if needed */
#define NUM_CPU				4

/* the type that will hold the simulation results:
 * cache size (K words), read miss rate, write miss rate, total miss rate,
 * etc. They're too many to count.
 */


typedef struct results_struct
{
	/* direct stats */
	uint32_t	reads[NUM_CPU],
				writes[NUM_CPU],
				invalidations[NUM_CPU],
				read_hits[NUM_CPU],
				write_hits[NUM_CPU],
				read_miss[NUM_CPU],
				write_miss[NUM_CPU],
				evictions[NUM_CPU],
				privates[NUM_CPU],
				shared_ro[NUM_CPU],
				shared_rw[NUM_CPU];

	/* derived stats */
	double		read_miss_rate[NUM_CPU],
				write_miss_rate[NUM_CPU],
				invalidation_rate[NUM_CPU],
				eviction_rate[NUM_CPU],
				privates_rate[NUM_CPU],
				shared_ro_rate[NUM_CPU],
				shared_rw_rate[NUM_CPU],
				cache_size;

	uint32_t	num_lines,
				line_size,

				total_reads,
				total_writes,
				total_invals,
				total_read_hits,
				total_read_miss,
				total_write_hits,
				total_write_miss,
				total_evictions,
				total_privates,
				total_shared_ro,
				total_shared_rw;
	double		total_read_miss_rate,
				total_read_hit_rate,
				total_write_miss_rate,
				total_write_hit_rate,
				total_invalidation_rate,
				total_eviction_rate,
				total_privates_rate,
				total_shared_ro_rate,
				total_shared_rw_rate;

	const char*	protocol;

} results_t;

#define STATE_M				2
#define STATE_E				3
#define STATE_S				1
#define STATE_I				0
typedef struct
{
	uint64_t tag = 0xdeadbeefdeadbeef; /* as the tag is always shifted right,
						  * this is an invalid one */
	uint64_t last_used_word = 0L; /* for calculating a rough estimate of false
								   * sharing */
	uint8_t state = STATE_I;
	bool used = false;
} cache_line_t;

typedef tuple<uint8_t, uint64_t, bool> data_line_t;

class CSimulator
{
private:
	/* data members */
	const char							*m_str_input_file;
	uint8_t								m_proto;
	bool								m_verbose;
	results_t							m_stats;

	/* Let the space-wasting begin */
	vector< data_line_t >				m_data;
	vector< data_line_t >::iterator		m_data_iterator;

	/* The caches are each a vector of cache lines */
	vector< cache_line_t >				m_cache[NUM_CPU];



public:
	CSimulator( );
	virtual ~CSimulator( );
	bool	init( int&, char** );
	bool	read_file( );
	void	run( );

	const char*			get_input_filename( ) const;
	const uint64_t		get_data_size( ) const;
	const results_t*	get_results( ) const;

private:
	int		init_( int&, char** );
	uint8_t	getState( int cache_num, uint32_t cache_line, uint32_t cache_tag );

	void	calc_stats( void );
	bool	handle_commands( uint8_t cmd, uint32_t curr_line );

	/* informs all processors (apart from the calling one) that an operation
	 * has taken place. Other processors will adjust state accordingly.
	 * @return: true if data available in another cache, false otherwise
	 */
	bool	inform( uint8_t informer, bool rw, uint64_t index, uint64_t tag );
};

#endif	/* CSIMULATOR_H */
