/*
 * File:   main.cpp
 * Author: Mihail Atanassov, s1018353
 *
 * Created on February 9, 2013, 6:40 PM
 */

#include <cstdlib>

#include "include/CSimulator.h"

using namespace std;

int main( int argc, char** argv )
{
	CSimulator sim;
	if ( !sim.init( argc, argv ) )
	{
		printf( "[Simulator]\tError during init, exiting.\n" );
		return EXIT_FAILURE;
	}

	if ( !sim.read_file( ) )
	{
		printf( "[Simulator]\tError reading file or file empty.\n" );
		return EXIT_FAILURE;
	}
	sim.run( );

	const char* filename = sim.get_input_filename();
	const uint64_t dataSize = sim.get_data_size( );
	const results_t* results = sim.get_results();

	/* sanity check */
// 	if ( results->total_read_miss == 0 && results->total_write_miss == 0 )
// 	{
// 		/* It's not technically possible to have 0 misses */
// 		printf(
// 			"[FAILURE]\tThis program's buggy, delete it NOW!\n" );
// 		return EXIT_FAILURE;
// 	}

	/* Print out the table of results */
	printf( "Filename:%51s \n", filename );
	printf( "Data set size:\t\t%10ld\n\n", dataSize );

	printf( "Protocol:\t\t%10s\n\n", results->protocol );

	printf( "Cache size (per CPU):\t%10.2lf K\n", results->cache_size );
	printf( "Cache lines (per CPU):\t%10d\n", results->num_lines );
	printf( "Words per cache line:\t%10d\n\n", results->line_size );

	printf( "Total read misses:\t%10d of %10d; %8.4lf %%\n",
			results->total_read_miss,
			results->total_reads,
			results->total_read_miss_rate );
	printf( "Total write misses:\t%10d of %10d; %8.4lf %%\n\n\n",
			results->total_write_miss,
			results->total_writes,
			results->total_write_miss_rate );

	/* Print per-cpu results */
	for( int i = 0; i < NUM_CPU; ++i )
	{
	printf( "\tCache %1d \n\n", i );

	printf( "Read misses:\t\t%10d of %10d; %8.4lf %%\n",
			results->read_miss[i],
			results->reads[i],
			results->read_miss_rate[i] );
	printf( "Write misses:\t\t%10d of %10d; %8.4lf %%\n",
			results->write_miss[i],
			results->writes[i],
			results->write_miss_rate[i] );
	printf( "Invalidations:\t\t%10d of %10d; %8.4lf %%\n",
			results->invalidations[i],
			results->writes[i],
			results->invalidation_rate[i]);
	printf( "Evictions:\t\t%10d of %10d; %8.4lf %%\n",
			results->evictions[i],
			results->reads[i] + results->writes[i],
			results->eviction_rate[i] );
	printf( "Private data:\t\t%10d of %10d; %8.4lf %%\n",
			results->privates[i],
			results->reads[i] + results->writes[i],
			results->privates_rate[i] );
	printf( "Shared RO:\t\t%10d of %10d; %8.4lf %%\n",
			results->shared_ro[i],
			results->reads[i] + results->writes[i],
			results->shared_ro_rate[i]);
	printf( "Shared R/W:\t\t%10d of %10d; %8.4lf %%\n\n\n",
			results->shared_rw[i],
			results->reads[i] + results->writes[i],
			results->shared_rw_rate[i] );
	}
	return EXIT_SUCCESS;
}
